//Contains all the endpoints for our application
//We separate the routes such that "index.js" file only contains information on the server

const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController")

//Routes
//Route to get all the tasks

router.get("/", (req, res) => {
	//Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
});

router.post("/", (req, res) => {
	console.log(req.body)
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
});

router.delete("/:id", (req, res) => {
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
});

//Route to update a task
router.put("/:id", (req, res) => {
	console.log(req.params.id)
	console.log(req.body)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});

//Use "module.exports" to export the router object to use in the "index.js" file
module.exports = router;