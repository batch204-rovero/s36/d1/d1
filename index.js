//Setep the dependencies
const express = require("express");
const mongoose = require("mongoose");

//This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 3000;
app.use(express.json());

//Database Connection
mongoose.connect("mongodb+srv://admin:admin123@batch204-roveroaron.k3oycwf.mongodb.net/B204-to-dos?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the cloud database"));

//Add the task route
app.use("/tasks", taskRoute);
//localhost:3000/tasks/

//Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}`));